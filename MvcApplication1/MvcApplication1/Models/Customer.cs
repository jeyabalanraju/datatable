﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class Customer
    {
        //public int CustomerID { get; set; }

        //public string Name { get; set; }

        //public string Address { get; set; }

        //public string Mobileno { get; set; }

        //public DateTime Birthdate { get; set; }

        //public string EmailID { get; set; }

        //public List<Customer> ShowallCustomer { get; set; }

        [Key]
        public int CustomerID { get; set; }

        [Required(ErrorMessage = "Enter Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Enter Address")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Enter Mobileno")]
        public string Mobileno { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Enter Birthdate")]
        public DateTime Birthdate { get; set; }

        [Required(ErrorMessage = "Enter EmailID")]
        public string EmailID { get; set; }

        public List<Customer> ShowAllCustomerDetails { get; set; }
    }
}